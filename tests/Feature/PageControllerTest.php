<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use JsonException;
use Tests\TestCase;

class PageControllerTest extends TestCase
{
    /**
     * @dataProvider validDataProvider
     * @throws JsonException
     */
    public function testUserCanCreatePageWithValidData(array $data): void
    {
        $image = UploadedFile::fake()->image('logo.jpg');
        $data['image'] = $image;

        $response = $this->post(route('pages.store'), $data);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('pages', [
            'title' => $data['title'],
            'website' => $data['website'],
        ]);
    }

    /**
     * @dataProvider inValidDataProvider
     * @throws JsonException
     */
    public function testUserCanNotCreatePageWithValidData(array $data): void
    {
        $image = UploadedFile::fake()->image('logo.jpg');
        $data['image'] = $image;

        $response = $this->post(route('pages.store'), $data);

        $response->assertSessionHasErrors();
        $this->assertDatabaseMissing('pages', [
            'title' => $data['title'],
            'website' => $data['website'],
        ]);
    }

    protected function validDataProvider(): array
    {
        return [
            [
                [
                    'color' => fake()->safeHexColor,
                    'title' => fake()->title,
                    'image' => fake()->imageUrl(360, 360, 'animals', true, 'cats'),
                    'website' => fake()->companyEmail,
                    'social_link' => fake()->domainName,
                ]
            ]
        ];
    }

    protected function inValidDataProvider(): array
    {
        return [
            [
                [
                    'color' => fake()->safeHexColor,
                    'title' => '',
                    'image' => fake()->imageUrl(360, 360, 'animals', true, 'cats'),
                    'website' => fake()->companyEmail,
                    'social_link' => fake()->domainName,
                ],
                [
                    'color' => fake()->safeHexColor,
                    'website' => fake()->companyEmail,
                    'social_link' => fake()->domainName,
                ],
                [
                    'color' => '',
                    'image' => '',
                    'website' =>'',
                    'social_link' => '',
                ]
            ]
        ];
    }
}
