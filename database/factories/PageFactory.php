<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Page>
 */
class PageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
                'color' => $this->faker->hexColor(),
                'title' => $this->faker->city,
                'website' => $this->faker->domainName(),
                'social_link' => $this->faker->companyEmail,
                'logo' => $this->faker->imageUrl(360, 360, 'animals', true, 'cats'),

        ];
    }
}
