import PageView from "./Components/Pages/PageView.vue";
import { createApp } from 'vue'
import PageComponent from './Components/Pages/PageComponent.vue'
import BootstrapVue3 from 'bootstrap-vue-3'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import InputColorPicker from "vue-native-color-picker";

require('./bootstrap');

import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        path: "/page-detail/:id",
        name: "PageDetail",
        component: PageView,
    },
    {
        path: "/",
        name: "main-page",
        component: PageComponent,
    },
]
const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(process.env.APP_URL),
    routes, // short for `routes: routes`
})


const app = createApp({
    components: {
        PageComponent,
    }
})

app.use(BootstrapVue3)
    .use(router)
    .use(VueSweetalert2)
    .use(InputColorPicker)
    .mount('#app');




