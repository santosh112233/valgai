import { createWebHistory, createRouter } from "vue-router";
import PageView from "./../Components/Pages/PageView.vue"

const index = [
    {
        path: "/page-detail/:id",
        name: "PageDetail",
        component: PageView,
    },

];

const router = createRouter({
    history: createWebHistory(),
    routes: index,
});

export default router;
