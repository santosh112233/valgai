<?php

namespace App\Providers;

use App\Services\ImageService\ImageUploaderService;
use App\Services\PageService\Contracts\PageServicable;
use App\Services\PageService\PageService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind('imageservice',function(){
            return new ImageUploaderService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(PageServicable::class, PageService::class);
    }
}
