<?php

namespace App\DTO;

class PageRequestData
{
    public  function  __construct(
        public readonly ?string $color,
        public readonly object $image,
        public readonly string $title,
        public readonly string $website,
        public readonly string $socialLink,
    ){

    }
}
