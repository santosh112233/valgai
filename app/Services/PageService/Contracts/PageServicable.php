<?php

namespace App\Services\PageService\Contracts;

use App\DTO\PageRequestData;
use App\Models\Page;
use Illuminate\Support\Collection;

interface PageServicable
{
    public function store(PageRequestData $pageRequestData): Page;

    public function gePages(): Collection;

    public function getPage(int $id): Page;
}
