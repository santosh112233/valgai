<?php

namespace App\Services\PageService;

use App\Models\Page;
use App\Services\ImageService\ImageServiceFacade;
use App\Services\PageService\Contracts\PageServicable;
use Illuminate\Support\Collection;

final class PageService implements PageServicable
{
    public function __construct(
        public Page $page
    )
    {

    }

    public function gePages(): Collection
    {
        return $this->page->get();
    }

    public function store($pageRequestData): Page
    {
        $imagePath = null;
        if ($pageRequestData) {
            $imagePath = ImageServiceFacade::uploadImage($pageRequestData->image, 'pagelogo');
        }
        return $this->page->create([
            'color' => $pageRequestData->color,
            'logo' => $imagePath,
            'title' => $pageRequestData->title,
            'website' => $pageRequestData->website,
            'social_link' => $pageRequestData->socialLink,
        ]);
    }

    public function getPage(int $id): Page
    {
        return $this->page->findOrFail($id);
    }

    public function deletePage(int $id): bool
    {
        return $this->page->findOrFail($id)->delete();
    }
}
