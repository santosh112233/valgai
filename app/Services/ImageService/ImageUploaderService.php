<?php

namespace  App\Services\ImageService;

final  class  ImageUploaderService
{
    public  function  uploadImage(object $image, string $path): string
    {
        $imageName = time().'.'.$image->extension();
        $image->move(public_path($path), $imageName);
        return $path.'/'.$imageName;
    }
}
