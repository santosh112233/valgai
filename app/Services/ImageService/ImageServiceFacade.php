<?php

namespace App\Services\ImageService;

use Illuminate\Support\Facades\Facade;

class ImageServiceFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'imageservice';
    }
}
