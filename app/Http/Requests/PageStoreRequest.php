<?php

namespace App\Http\Requests;

use App\DTO\PageRequestData;
use Illuminate\Foundation\Http\FormRequest;

class PageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'image' => 'required|image',
            'title' => 'required',

        ];
    }

    public  function  Dto(): PageRequestData
    {
        return  new PageRequestData(
            color: $this->color,
            image: $this->image,
            title:  $this->title,
            website: $this->website,
            socialLink:  $this->social_link,
        );
    }
}
