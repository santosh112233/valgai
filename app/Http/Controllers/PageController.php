<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageStoreRequest;
use App\Http\Resources\PageResource;
use App\Services\PageService\Contracts\PageServicable;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

final class PageController extends Controller
{
    public function __construct(
        public PageServicable $pageServicable
    ){
    }


    protected function index()
    {
        try {
            return PageResource::collection($this->pageServicable->gePages());

        } catch (Exception $e) {
            return response(['errors' => $e->getMessage()], 202);
        }
    }

    protected function store(PageStoreRequest $request): JsonResponse|string
    {
        try {
            DB::beginTransaction();
            $pageUser = $this->pageServicable->store($request->Dto());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $error = $e->getMessage();
        } finally {
            return $error ??
                (new PageResource($pageUser))
                    ->response();
                $pageUser ??
                response(['errors' => $error], 202);
        }
    }

    protected function show(int $id)
    {
        try {
            $pageUser = $this->pageServicable->getPage($id);
        } catch (Exception $e) {
            $error = $e->getMessage();
        } finally {
            return $error ??
                (new PageResource($pageUser))
                    ->response();
                $pageUser ??
                response(['errors' => $error], 202);
        }
    }

    protected function destroy(int $id)
    {
        try {
            $this->pageServicable->deletePage($id);
        } catch (Exception $exception) {
            return response(['error' => $exception->getMessage(), 202]);
        }
        return response(204);
    }
}
